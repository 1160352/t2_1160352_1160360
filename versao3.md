library("seqinr")
gorilla <- read.fasta(file = "C:\\Users\\AvelhaMina\\Documents\\ISEP\\3ºAno\\MCBIOS\\gorilla.fasta")
gorillaseq<-toupper(c2s(gorilla[[1]]))

homo <- read.fasta(file = "C:\\Users\\AvelhaMina\\Documents\\ISEP\\3ºAno\\MCBIOS\\homo.fasta")
homoseq<-toupper(c2s(homo[[1]]))

dragao <- read.fasta(file = "C:\\Users\\AvelhaMina\\Documents\\ISEP\\3ºAno\\MCBIOS\\dragao.fasta")
dragaoseq<-toupper(c2s(dragao[[1]]))

library(Biostrings)


#####################################################################################################
# exercicio 3.1 

gorillaseq<-gorilla[[1]]
dragaoseq <- dragao[[1]]
homoseq <- homo[[1]]

pal1<-count(gorillaseq,1,freq=T)
pal2<-count(dragaoseq,1,freq=T)
pal3<-count(homoseq,1,freq=T)
pie((pal1)) # grafico circular
pie((pal2))
pie((pal3))

# na espécie gorilla a timina é o nucleótido mais frequente
# na espécie dragão o nucleótido mais frequente é a citosina
# na espécie homo a timina é mais frequente.

#####################################################################################################


# exercicio 3.2
dotPlot(gorillaseq, dragaoseq) # compara o gorila com o dragão
dotPlot(gorillaseq, homoseq) # compara o gorilla com o homo
dotPlot(homoseq, dragaoseq) # compara o homo com o dragão

#####################################################################################################

gorillaseq<-toupper(c2s(gorillaseq))
homoseq<-toupper(c2s(homoseq))
dragaoseq<-toupper(c2s(dragaoseq))

# exercicio 3.2.2
sigma <- nucleotideSubstitutionMatrix(match = 1, mismatch = -1, baseOnly = TRUE, type = "DNA")

#ALINHAMENTO GLOBAL GORILLA-HOMOSAPIENS
gorila_homo_ali_global<-pairwiseAlignment(gorillaseq, homoseq, substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="global",
                    scoreOnly = F)
gorila_homo_ali_global

#ALINHAMENTO LOCAL GORILLA-HOMOSAPIENS
gorila_homo_ali_local <- pairwiseAlignment(gorillaseq, homoseq, substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="local",
                  scoreOnly = F)
gorila_homo_ali_local

#ALINHAMENTO GLOBAL HOMOSAPIENS-DRAGAO
homo_dragao_ali_global<-pairwiseAlignment(homoseq, dragaoseq, substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="global",
                  scoreOnly = F)
homo_dragao_ali_global
#ALINHAMENTO LOCAL HOMOSAPIENS-DRAGAO
homo_dragao_ali_local <- pairwiseAlignment(homoseq, dragaoseq, substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="local",
                  scoreOnly = F)
homo_dragao_ali_local

#ALINHAMENTO GLOBAL GORILLA-DRAGAO
gorila_dragao_ali_global <- pairwiseAlignment(gorillaseq, dragaoseq, substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="global",
                  scoreOnly = F)
gorila_dragao_ali_global
#ALINHAMENTO LOCAL GORILLA-DRAGAO
gorila_dragao_ali_local <- pairwiseAlignment(gorillaseq, dragaoseq, substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="local",
                  scoreOnly = F)
gorila_dragao_ali_local

#segundo o score obtidos nos alinhamentos anteriores, o dragao é mais compativel com a especie de gorila analisada
#quando comparado com o alinhamento obtido com a sequencia correspondente ao homosapiens. No entanto, é de ter em conta que
#o tamanho das sequencias tem impacto no resultado obtido e, neste caso, a sequencia do dragao é bastante maior que
#a do homosapiens o que faz com que o alinhamento entre este e o gorila tenha mais gaps e menos matches. 
#podemos ainda ver que os scores obtidos no alinhamento local é sempre maior que os obtidos por alihamento global

#SIGNIFICANCIA ESTATISTICA 

generateSeqsWithMultinomialModel <- function(inputsequence, X)
{
  # Change the input sequence into a vector of letters
  require("seqinr") # This function requires the SeqinR package.
  inputsequencevector <- s2c(inputsequence)
  # Find the frequencies of the letters in the input sequence "inputsequencevector":
  mylength <- length(inputsequencevector)
  mytable <- table(inputsequencevector)
  # Find the names of the letters in the sequence
  letters <- rownames(mytable)
  numletters <- length(letters)
  probabilities <- numeric() # Make a vector to store the probabilities of letters
  for (i in 1:numletters)
  {
    letter <- letters[i]
    count <- mytable[[i]]
    probabilities[i] <- count/mylength
  }
  # Make X random sequences using the multinomial model with probabilities "probabilities"
  seqs <- numeric(X)
  for (j in 1:X)
  {
    seq <- sample(letters, mylength, rep=TRUE, prob=probabilities) # Sample with replacement
    seq <- c2s(seq)
    seqs[j] <- seq
  }
  # Return the vector of random sequences
  return(seqs)
}

randomseq <- generateSeqsWithMultinomialModel(gorillaseq, 1000)

#comparando com homosapiens
randomscores <- double(1000) # Create a numeric vector with 1000 elements
for (i in 1:1000)
{
  score <- pairwiseAlignment(homoseq, randomseq[i], substitutionMatrix = sigma,
                             gapOpening = 0, gapExtension = -1, scoreOnly = TRUE)
  randomscores[i] <- score
}

hist(randomscores, col="red") # Draw a red histogram
#score do alinhamento global foi -2282
sum(randomscores >= -2282)
#o p-value obtido foi de 1(1000/1000) logo como 1>0.05, o score obtido nao é estatisticamente significativo

#comparando com o dragao 
for (i in 1:1000)
{
  score <- pairwiseAlignment(dragaoseq, randomseq[i], substitutionMatrix = sigma,
                             gapOpening = 0, gapExtension = -1, scoreOnly = TRUE)
  randomscores[i] <- score
}

hist(randomscores, col="red")
#score do alinhamento global foi -53
sum(randomscores >= -53)
#o p-value obtido foi de 0.898(898/1000) logo como 0.898>0.05, o score obtido nao é estatisticamente significativo

#####################################################################################################


# exercicio 3.2.3

printPairwiseAlignment <- function(alignment, chunksize, returnlist=FALSE)
{
  require(Biostrings) # This function requires the Biostrings package
  seq1aln <- pattern(alignment) # Get the alignment for the first sequence
  seq2aln <- subject(alignment) # Get the alignment for the second sequence
  alnlen <- nchar(seq1aln) # Find the number of columns in the alignment
  starts <- seq(1, alnlen, by=chunksize)
  n <- length(starts)
  seq1alnresidues <- 0
  seq2alnresidues <- 0
  for (i in 1:n) {
    chunkseq1aln <- substring(seq1aln, starts[i], starts[i]+chunksize-1)
    chunkseq2aln <- substring(seq2aln, starts[i], starts[i]+chunksize-1)
    # Find out how many gaps there are in chunkseq1aln:
    gaps1 <- countPattern("-",chunkseq1aln) # countPattern() is from Biostrings package
    # Find out how many gaps there are in chunkseq2aln:
    gaps2 <- countPattern("-",chunkseq2aln) # countPattern() is from Biostrings package
    # Calculate how many residues of the first sequence we have printed so far in the alignment:
    seq1alnresidues <- seq1alnresidues + chunksize - gaps1
    # Calculate how many residues of the second sequence we have printed so far in the alignment:
    seq2alnresidues <- seq2alnresidues + chunksize - gaps2
    if (returnlist == 'FALSE')
    {
      print(paste(chunkseq1aln,seq1alnresidues))
      print(paste(chunkseq2aln,seq2alnresidues))
      print(paste(' '))
    }
  }
  if (returnlist == 'TRUE')
  {
    vector1 <- s2c(substring(seq1aln, 1, nchar(seq1aln)))
    vector2 <- s2c(substring(seq2aln, 1, nchar(seq2aln)))
    mylist <- list(vector1, vector2)
    return(mylist)
  }
}


printPairwiseAlignment(gorila_homo_ali_global, 50, returnlist=FALSE)
#quando observamos o alinhamento das sequencias do gorila e homosapiens, verificamos que esta contem quase
#exclusivamnte gaps e matches o que nos leva a pensar que caso a sequencia do homosapiens fosse maior em termos de 
#numero de nucleotidos, que o resultado obtido seria muito mais proximo do esperado
printPairwiseAlignment(gorila_dragao_ali_local, 50, returnlist=FALSE)

printPairwiseAlignment(homo_dragao_ali_global, 50, returnlist=FALSE)



#####################################################################################################



# exercicio 3.3

aliclustal <- read.alignment(file = "C:\\Users\\AvelhaMina\\Documents\\ISEP\\3ºAno\\MCBIOS\\aliclustal.phy", format = "phylip")
require("ape")

#ver o alinhamento em porções
printMultipleAlignment <- function(alignment, chunksize=60)
{
  # this function requires the Biostrings package
  require("Biostrings")
  # find the number of sequences in the alignment
  numseqs <- alignment$nb
  # find the length of the alignment
  alignmentlen <- nchar(alignment$seq[[1]])
  starts <- seq(1, alignmentlen, by=chunksize)
  n <- length(starts)
  # get the alignment for each of the sequences:
  aln <- vector()
  lettersprinted <- vector()
  for (j in 1:numseqs)
  {
    alignmentj <- alignment$seq[[j]]
    aln[j] <- alignmentj
    lettersprinted[j] <- 0
  }
  # print out the alignment in blocks of 'chunksize' columns:
  for (i in 1:n) { # for each of n chunks
    for (j in 1:numseqs)
    {
      alnj <- aln[j]
      chunkseqjaln <- substring(alnj, starts[i], starts[i]+chunksize-1)
      chunkseqjaln <- toupper(chunkseqjaln)
      # Find out how many gaps there are in chunkseqjaln:
      gapsj <- countPattern("-",chunkseqjaln) # countPattern() is from Biostrings package
      # Calculate how many residues of the first sequence we have printed so far in the alignment:
      lettersprinted[j] <- lettersprinted[j] + chunksize - gapsj
      print(paste(chunkseqjaln,lettersprinted[j]))
    }
    print(paste(' '))
  }
}
printMultipleAlignment(aliclustal, chunksize=60)


dist.dna(as.DNAbin(aliclustal))#distancia entre especies

#####################################################################################################

# exercicio 3.4 ARVORE NAO ENRAIZADA

unrootedNJtree <- function(alignment,type)
{
  # this function requires the ape and seqinR packages:
  require("ape")
  require("seqinr")
  # define a function for making a tree:
  makemytree <- function(alignmentmat)
  {
    alignment <- ape::as.alignment(alignmentmat)
    if (type == "protein")
    {
      mydist <- dist.alignment(alignment)
    }
    else if (type == "DNA")
    {
      alignmentbin <- as.DNAbin(alignment)
      mydist <- dist.dna(alignmentbin)
    }
    mytree <- nj(mydist)
    mytree <- makeLabel(mytree, space="") # get rid of spaces in tip names.
    return(mytree)
  }
  # infer a tree
  mymat <- as.matrix.alignment(alignment)
  mytree <- makemytree(mymat)
  # bootstrap the tree
  myboot <- boot.phylo(mytree, mymat, makemytree)
  # plot the tree:
  plot.phylo(mytree,type="u") # plot the unrooted phylogenetic tree
  nodelabels(myboot,cex=0.7) # plot the bootstrap values
  mytree$node.label <- myboot # make the bootstrap values be the node labels
  return(mytree)
}

library("ape")
aln <- read.alignment(file = "C:\\Users\\AvelhaMina\\Documents\\ISEP\\3ºAno\\MCBIOS\\aliclustal.phy", format = "phylip")

unrtree <- unrootedNJtree(aln, "DNA")




#####################################################################################################


# exercicio 3.4 ARVORE ENRAIZADA

rootedNJtree <- function(alignment, theoutgroup, type)
{
  # load the ape and seqinR packages:"ape" and "seqinr"
  # define a function for making a tree:
  makemytree <- function(alignmentmat, outgroup=theoutgroup)
  {
    alignment <- ape::as.alignment(alignmentmat)
    if (type == "protein")
    {
      mydist <- dist.alignment(alignment)
    }
    else if (type == "DNA")
    {
      alignmentbin <- as.DNAbin(alignment)
      mydist <- dist.dna(alignmentbin)
    }
    mytree <- nj(mydist)
    mytree <- makeLabel(mytree, space="") # get rid of spaces in tip names.
    myrootedtree <- root(mytree, outgroup, r=TRUE)
    return(myrootedtree)
  }
  # infer a tree
  mymat <- as.matrix.alignment(alignment)
  myrootedtree <- makemytree(mymat, outgroup=theoutgroup)
  # bootstrap the tree
  myboot <- boot.phylo(myrootedtree, mymat, makemytree)
  # plot the tree:
  plot.phylo(myrootedtree, type="p") # plot the rooted phylogenetic tree
  nodelabels(myboot,cex=0.7) # plot the bootstrap values
  mytree$node.label <- myboot # make the bootstrap values be the node labels
  return(mytree)
}

rtree <- rootedNJtree(aln,"AF407510.1",type="DNA")



#####################################################################################################

# exercicio 4

#começamos por fazer um estudo do estado perdominante na nossa sequencia (gorila) com o recurso a sliding windows
#CONTEUDO AT/GC DA SEQUENCIA DO GORILLA
slidingwindowplotAT <- function(windowsize, inputseq)
{
  starts <- seq(1, length(inputseq)-windowsize)
  n <- length(starts) # encontra o comprimento do vector"starts"
  chunkATs <- numeric(n) # faz um vetor do mesmo comprimento que o vetor starts mas só contendo zeros
  for (i in 1:n) {
    chunk <- inputseq[starts[i]:(starts[i]+windowsize-1)]
    chunkAT <- count(chunk,1,freq=T)["a"]+count(chunk,1,freq=T)["t"]
    chunkATs[i] <- chunkAT
  }
  plot(starts,chunkATs,type="l",xlab="Posição inicial do nucleótideo",ylab="Conteúdo AT")
}

seqgorilla <- gorilla[[1]]
slidingwindowplotAT(4000,seqgorilla)
#a sequencia é sempre rica em at pois, no grafico é possivel verificar que a frequencia de a+t é sempre maior que 0.5

#apos verificar que a nossa sequencia era sempre AT rich, começamos a definir e gerar sequencias com as caracteristicas da nossa 
#MATRIZ TRANSIÇÃO
states              <- c("AT-rich", "GC-rich") # Define the names of the states
ATrichprobs         <- c(0.7, 0.3)             # As probabilidades de AT-rico foram alteradas consoante o que faria mais sentido no nosso caso
GCrichprobs         <- c(0.9, 0.1)             # As probabilidades de GC-rich foram alteradas consoante o que faria mais sentido no nosso caso
thetransitionmatrix <- matrix(c(ATrichprobs, GCrichprobs), 2, 2, byrow = TRUE) # Create a 2 x 2 matrix
rownames(thetransitionmatrix) <- states
colnames(thetransitionmatrix) <- states
thetransitionmatrix  


#MATRIZ EMISSÃO
nucleotides         <- c("A", "C", "G", "T")   # Define the alphabet of nucleotides
ATrichstateprobs    <- c(0.30, 0.19, 0.19, 0.32) # As probabilidades dos nucleotidos foram alteradas para as frequencias registadas na nossa sequencia correspondente ao gorila
GCrichstateprobs    <- c(0.1, 0.41, 0.39, 0.1) 
theemissionmatrix <- matrix(c(ATrichstateprobs, GCrichstateprobs), 2, 4, byrow = TRUE) # Create a 2 x 4 matrix
rownames(theemissionmatrix) <- states
colnames(theemissionmatrix) <- nucleotides
theemissionmatrix


generatehmmseq <- function(transitionmatrix, emissionmatrix, initialprobs, seqlength)
{
  seqe<-c()
  nucleotides     <- c("A", "C", "G", "T")   # Define the alphabet of nucleotides
  states          <- c("AT-rich", "GC-rich") # Define the names of the states
  mysequence      <- character()             # Create a vector for storing the new sequence
  mystates        <- character()             # Create a vector for storing the state that each position in the new sequence
  # was generated by
  # Choose the state for the first position in the sequence:
  firststate      <- sample(states, 1, rep=TRUE, prob=initialprobs)
  # Get the probabilities of the current nucleotide, given that we are in the state "firststate":
  probabilities   <- emissionmatrix[firststate,]
  # Choose the nucleotide for the first position in the sequence:
  firstnucleotide <- sample(nucleotides, 1, rep=TRUE, prob=probabilities)
  mysequence[1]   <- firstnucleotide         # Store the nucleotide for the first position of the sequence
  mystates[1]     <- firststate              # Store the state that the first position in the sequence was generated by
  
  for (i in 2:seqlength)
  {
    prevstate    <- mystates[i-1]           # Get the state that the previous nucleotide in the sequence was generated by
    # Get the probabilities of the current state, given that the previous nucleotide was generated by state "prevstate"
    stateprobs   <- transitionmatrix[prevstate,]
    # Choose the state for the ith position in the sequence:
    state        <- sample(states, 1, rep=TRUE, prob=stateprobs)
    # Get the probabilities of the current nucleotide, given that we are in the state "state":
    probabilities <- emissionmatrix[state,]
    # Choose the nucleotide for the ith position in the sequence:
    nucleotide   <- sample(nucleotides, 1, rep=TRUE, prob=probabilities)
    mysequence[i] <- nucleotide             # Store the nucleotide for the current position of the sequence
    mystates[i]  <- state                   # Store the state that the current position in the sequence was generated by
  }
  
  for (i in 1:length(mysequence))
  {
    nucleotide   <- mysequence[i]
    state        <- mystates[i]
    print(paste("Position", i, ", State", state, ", Nucleotide = ", nucleotide))
    seqe[i]<-nucleotide
  }
  return(seqe) #ao adicionar o vetor "seqe" é-nos possivel alinhar a sequencia obtida com a do gorila
}

#definiram-se as probabilidades iniciais, neste caso, como querems uma sequencia rica em at, as probabilidades iniciais favoreciam esse estado
theinitialprobs <- c(0.9,0.1)
at<-generatehmmseq(thetransitionmatrix, theemissionmatrix, theinitialprobs, length(seqgorilla))  #geramos uma sequencia com aproximadamente o mesmo numero de nucleotidos que a do gorila

#de seguida alinhamos a sequencia obtida com a do gorila, por forma a avaliar o score da mesma
at2<- toupper(c2s(at))
gorilla_at2<-pairwiseAlignment(gorillaseq,at2 , substitutionMatrix = sigma, gapOpening = 0, gapExtension = -1, type="global",
                  scoreOnly = F)
gorilla_at2 # como era de esperar, o score deste alinhamento é bastante maior que todos os alinhamentos feitos anteriormente, uma vez que a sequencia at2 foi construida à semelhança da sequencia do gorila
printPairwiseAlignment(gorilla_at2, 50, returnlist=FALSE) # recorrendo à função implementada anteriormente, podeos imprimir o alinhamento por partes com 50 nucleotidos cada.
#ao analizar as secçoes, podemos verificar que existem bastantes matches e que o numero de gaps diminuiu.
#no entanto este factor deve-se ao comprimento da sequencia gerada que, ao contrario das outras, tem tamanho igual ao da seuquencia do gorila. 
